require(dotenv).config()

import express from 'express'
import router from './scr/router'
import errorHandler from './scr/middleWare/errorHandler'
import dotenv from 'dotenv'
import verifyToken from './middleWare/jwtVerify'



const PORT = process.env.PORT

const app = express()

app.use(express.json())

app.use(router)

app.use(errorHandler)

app.post('/login', (req, res) => {
    const username = req.body.username
    const password = req.body.password

    if (username && password) {
        
        const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '3m'})

        return res.json({token})
    }
    return res.status(400).json({error: "incorrect credentials provided"})
})

app.use(verifyToken)

app.get('/protected', (req, res) => {

    res.json({message: "Successful", user: req.user})
})


app.listen(PORT, () => console.log(`Server started at http://localhost:${PORT}`))

