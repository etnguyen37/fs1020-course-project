require(dotenv).config()

import util from 'util'
import fs from 'fs'
import path from 'path'


const readFile = util.promisify(fs.readFile);

const writeFile = util.promisify(fs.writeFile);


const entriesDB = path.resolve(process.env.entriesDbRoute);

const usersDB = path.resolve(process.env.usersDbRoute)


async function readEntries () {

  const json = await readFile(entriesDB);

  return JSON.parse(json);
}


async function readUsers () {

    const json = await readFile(usersDB);

    return JSON.parse(json);
  }


  async function writeUsers (users) {
   
    const json = JSON.stringify(users, null, 2);
  
    return writeFile(usersDB, json);
  }
  

  async function writeEntries (entries ) {
   
    const json = JSON.stringify(entries, null, 2);
  
    return writeFile(entriesDB, json);
  }


export {
    readEntries,
    readUsers,
    writeEntries,
    writeUsers
}
