function errorHandler(err,req,res,next) {
    if (res.headersSent) {
        return next(err);
    }
    console.error (err.stack)
    return res.status(500).send({message:"Not Found"})
}

export default errorHandler 