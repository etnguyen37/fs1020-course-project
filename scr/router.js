require(dotenv).config()

import express from "express";
import users from "../Data/users";
import entries from "../Data/entries";
import { v4 as uuidv4 } from "uuid";
import verifyToken from "./middleWare/jwtVerify"
import {hash} from 'bcrypt';
import {readEntries,readUsers, writeEntries, writeUsers } from './util/jsonHandler'

const router = express.Router()

let jwt = require("jsonwebtoken")

const bcrypt = require('bcrypt');

const validationCheck = (req, res, next) => {
    const errors = []
    if (req.body.name == null) {
        errors.push("name")
    } 
    
    if (req.body.email == null) {
        errors.push("email")
    }

    if (req.body.phoneNumber == null){
        errors.push("phone")
    } 
    
    if (req.body.content == null) {
        errors.push("content")
    }
    
    if (errors.length > 0) {
       return res.status(400).send({message: "Validation error", errors})
    }
    next()
}


router.post("/contact_form/entries", validationCheck, (req,res)=> {
    
    let newEntries = {

        "id":uuidv4(),
        "name": req.body.name,
        "email": req.body.email,
        "phoneNumber": req.body.phoneNumber,
        "content": req.body.content
    }

    readEntries().then((entriesJson) =>{

        entriesJson.push(newEntries)

        writeEntries().then((entriesJson) => {
            
            return res.status(201).send(entriesJson)
        })

    })

})


router.post("/users", validationCheck, (req,res) => {

    let newUsers = {

        "id":uuidv4(),
        "name": req.body.name,
        "password": req.body.password,
        "email": req.body.email,
    }
   
bcrypt.hash(newUsers.password, `${process.env.saltRounds}`.then(function (err, hash) {
    
newUserPassword = hash


readUsers().then((usersJson) => {

    usersJson.push(newUsers)

    writeUsers().then(usersJson) 

    return res.status(201).send(usersJson)
    

})

}));

})


router.post("/auth", (req,res) => {

    let users = {
        "password": req.body.password,
        "email": req.body.email,
    }

let token = jwt.sign(users, "shhhhh")

users.push(users)

return res.status(201).send(token)}
)


router.get("/contact_form/entries", verifyToken, (req,res) => {

    readEntries().then((entriesJson))

    return res.status(201).send(entriesJson)
})


router.get("/contact_form/entries/:id", verifyToken, (req,res) => {
    const id = req.params.id

    const entryIndex = entries.findIndex(entry => entry.id == id) 

    if (entryIndex == -1) 
    {       
    return res.status(404).json( { error: `entry ${id} not found`})
}
    return res.json(entries[entryIndex])
})


export default router;