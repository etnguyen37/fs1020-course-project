# FS1020 Course Project

Part 1

Gitlab repository is set up, including ignoring node_modules

Express is running on a high port number such as 3000, configurable from the environment.
Readme file contains information about the project, as well as how to start the application as well as configure the environment.
npm is used to add any relevant packages

Part 2

Be sure to pull your merged changes once merged, before creating a new branch off of master!
Routes are setup and semantically correct, and respond with a valid and applicable response as define above.
Express default error handling middleware is setup where any route not found should return back the appropriate status code (Not Found) and the following response: {"message": "not found"}.
Express JSON parsing middleware is setup.
Expectation is that the project is organized with the appropriate modules, but is not expected to be saving anything to a JSON file. For instance you will have the route files setup, however your routes may only have validation logic attached, and nothing more.
Your route is protected using token-based authentication (JWT), as mentioned above. You can use a hard-coded password for JWT generation. Part 3 will replace this.

Part 3

There are two resources identified in this project: users and entries. There should exist two json files, in a folder called data located where package.json is. These json files should be ignored from git as well.
Create a module that can read and write to the two JSON files. If file does not exist, your module should first create the file in the proper location
Update your endpoints from Part 2 to now read and write to the JSON as required by the route definitions
Update your code to check for username/password from the users.json file (passwords should not be stored as plaintext, but hashed)
Open a merge request, and assign the instructor

 "name": "fs1020-course-project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "type": "module
  author": "Tran Uyen Nhi Nguyen",
  "license": "ISC"